/**
 * @license MIT
 * @license http://opensource.org/licenses/MIT Massachusetts Institute of Technology
 * @copyright 2014 Patric Gutersohn
 * @author Patric Gutersohn
 * @example index.html BDT in action.
 * @link http://bdt.pguso.de Documentation
 * @version 1.0.0
 *
 * @summary BDT - Bootstrap Data Tables
 * @description sorting, paginating and search for bootstrap tables
 */

(function (jQuery) {
    "use strict";

    /**
     * @type {number}
     */
    var actualPage = 1;
    /**
     * @type {number}
     */
    var pageCount = 0;
    /**
     * @type {number}
     */
    var pageRowCount = 0;
    /**
     * @type {string}
     */
    var pages = '';
    /**
     * @type {object}
     */
    var obj = null;
    /**
     * @type {boolean}
     */
    var activeSearch = false;
    /**
     * @type {string}
     */
    var arrowUp = '';
    /**
     * @type {string}
     */
    var arrowDown = '';
    /**
     * @type {string}
     */
    var searchFormClass = '';
    /**
     * @type {string}
     */
    var pageFieldText = '';
    /**
     * @type {string}
     */
    var searchFieldText = '';

    var pageRowStart=1;

    AJS.$.fn.bdt = function (options, callback) {

        var settings = AJS.$.extend({
            pageRowCount: 50,
            arrowDown: 'fa-angle-down',
            arrowUp: 'fa-angle-up',
            searchFormClass: 'pull-left search-form',
            pageFieldText: AJS.I18n.getText('status.report.entries.per.page'),
            searchFieldText: AJS.I18n.getText('common.concepts.search')
        }, options);

        /**
         * @type {object}
         */
        var tableBody = null;

        return this.each(function () {
            obj = AJS.$(this).addClass('bdt');
            tableBody = obj.find("tbody");
            pageRowCount = settings.pageRowCount;
            arrowDown = settings.arrowDown;
            arrowUp = settings.arrowUp;
            searchFormClass = settings.searchFormClass;
            pageFieldText = settings.pageFieldText;
            searchFieldText = settings.searchFieldText;

            /**
             * search input field
             */
            obj.before(
                AJS.$('<div/>')
                    .addClass('table-header')
                    .append(
                        AJS.$('<form/>')
                            .addClass(searchFormClass)
                            .attr('role', 'form')
                            .append(
                                AJS.$('<div/>')
                                    .addClass('form-group')
                                    .append(
                                        AJS.$('<input/>')
                                            .addClass('form-control')
                                            .attr('id', 'search')
                                            .attr('placeholder', searchFieldText)
                                    )
                            )
                    )
                    .append(
                        AJS.$('<div/>')
                            .addClass('pull-left')
                            .append(
                                AJS.$('<form/>')
                                    .addClass('form-horizontal')
                                    .attr('id', 'page-rows-form')
                                    .append(AJS.$('<label/>')
                                        .addClass('pull-left control-label')
                                        .attr("style","margin:5px 10px 5px 10px")
                                        .text(pageFieldText)
                                    )
                                    .append(
                                        AJS.$('<div/>')
                                            .addClass('pull-left')
                                            .append(
                                                AJS.$('<select/>')
                                                    .addClass('js-basic-single')
                                                    .attr('id','linePerPage')
                                                    .append(
                                                        AJS.$('<option>', {
                                                            value: 10,
                                                            text: 10
                                                        })
                                                    )
                                                    .append(
                                                        AJS.$('<option>', {
                                                            value: 20,
                                                            text: 20
                                                        })
                                                    )
                                                    .append(
                                                        AJS.$('<option>', {
                                                            value: 50,
                                                            text: 50,
                                                            selected: 'selected'
                                                        })
                                                    )
                                                    .append(
                                                        AJS.$('<option>', {
                                                            value: 100,
                                                            text: 100
                                                        })
                                                    )

                                            )
                                    )
                            )
                    )
            );

            function updateFooter(pageRowStart){

                var totalRows = Math.ceil(tableBody.children('tr').length);
                var pageRowTo = actualPage * pageRowCount
                             if (totalRows<=pageRowTo){
                                var pageRowTo = totalRows
                             }

                AJS.$("#footer-rows-info").text(AJS.I18n.getText("status.report.rows.info", pageRowStart.toString(),pageRowTo.toString() , totalRows.toString()))

            };

            /**
             * select field for changing row per page
             */

            obj.after(
                AJS.$('<div/>')
                    .attr('id', 'table-footer')
                    .append(
                        AJS.$('<div/>')
                            .addClass('pull-left table-info')
                            .append('<span id="footer-rows-info"></span>')
                            //.text(AJS.I18n.getText("status.report.rows.info", showRows.toString() , totalRows.toString()))

                    )

            );
            updateFooter(1);
            if (tableBody.children('tr').length > pageRowCount) {
                setPageCount(tableBody);
                addPages();
                paginate(tableBody, actualPage);
            }

            searchTable(tableBody);
            sortColumn(obj, tableBody);

            AJS.$('body').on('click', '.pagination li', function (event) {
                var listItem;

                if (AJS.$(event.target).is("a")) {
                    listItem = AJS.$(event.target).parent();
                } else {
                    listItem = AJS.$(event.target).parent().parent();
                }

                var page = listItem.data('page');

                if (!listItem.hasClass("disabled") && !listItem.hasClass("active")) {
                    paginate(tableBody, page);
                }
                updateFooter(pageRowCount*(actualPage-1)+1)
            });

            AJS.$('#page-rows-form').on('change', function () {
                var options = AJS.$(this).find('select');
                pageRowCount = options.val();

                setPageCount(tableBody);
                addPages();
                paginate(tableBody, 1);
                updateFooter(pageRowStart)
            });

        });

        /**
         * the main part of this function is out of this thread http://stackoverflow.com/questions/3160277/jquery-table-sort
         * @author James Padolsey http://james.padolsey.com
         * @link http://jsfiddle.net/spetnik/gFzCk/1953/
         * @param obj
         */
        function sortColumn(obj) {
            var table = obj;
            var oldIndex = 0;

            obj
                .find('thead th')
                .wrapInner('<span class="sort-element"/>')
                .append(
                    AJS.$('<span/>')
                        .addClass('sort-icon fa')
                )
                .each(function () {

                    var th = AJS.$(this);
                    var thIndex = th.index();
                    var inverse = false;
                    var addOrRemove = true;

                    th.click(function () {
                        if(!AJS.$(this).hasClass('disable-sorting')) {
                            if(AJS.$(this).find('.sort-icon').hasClass(arrowDown)) {
                                AJS.$(this)
                                    .find('.sort-icon')
                                    .removeClass( arrowDown )
                                    .addClass(arrowUp);

                            } else {
                                AJS.$(this)
                                    .find('.sort-icon')
                                    .removeClass( arrowUp )
                                    .addClass(arrowDown);
                            }

                            if(oldIndex != thIndex) {
                                obj.find('.sort-icon').removeClass(arrowDown);
                                obj.find('.sort-icon').removeClass(arrowUp);

                                AJS.$(this)
                                    .find('.sort-icon')
                                    .toggleClass( arrowDown, addOrRemove );
                            }

                            table.find('td').filter(function () {

                                return AJS.$(this).index() === thIndex;

                            }).sortElements(function (a, b) {

                                return AJS.$.text([a]) > AJS.$.text([b]) ?
                                    inverse ? -1 : 1
                                    : inverse ? 1 : -1;

                            }, function () {

                                // parentNode is the element we want to move
                                return this.parentNode;

                            });

                            inverse = !inverse;
                            oldIndex = thIndex;
                        }
                    });

                });
        }

        /**
         * create li elements for pages
         */
        function addPages() {
            AJS.$('#table-nav').remove();
            pages = AJS.$('<ul/>');

            AJS.$.each(new Array(pageCount), function (index) {
                var additonalClass = '';
                var page = AJS.$();

                if ((index + 1) == 1) {
                    additonalClass = 'active';
                }

                pages
                    .append(AJS.$('<li/>')
                        .addClass(additonalClass)
                        .data('page', (index + 1))
                        .append(
                            AJS.$('<a/>')
                                .text(index + 1)
                        )
                    );
            });

            /**
             * pagination, with pages and previous and next link
             */
            AJS.$('#table-footer')
                .addClass('row')
                .append(
                    AJS.$('<nav/>')
                        .addClass('pull-right')
                        .attr('id', 'table-nav')
                        .append(
                            pages
                                .addClass('pagination pull-right')
                                .prepend(
                                    AJS.$('<li/>')
                                        .addClass('disabled')
                                        .data('page', 'previous')
                                        .append(
                                            AJS.$('<a href="#" />')
                                                .append(
                                                    AJS.$('<span/>')
                                                        .attr('aria-hidden', 'true')
                                                        .html('&laquo;')
                                                )
                                                .append(
                                                    AJS.$('<span/>')
                                                        .addClass('sr-only')
                                                        .text(AJS.I18n.getText('status.report.previous'))
                                                )
                                        )
                                )
                                .append(
                                    AJS.$('<li/>')
                                        .addClass('disabled')
                                        .data('page', 'next')
                                        .append(
                                            AJS.$('<a href="#" />')
                                                .append(
                                                    AJS.$('<span/>')
                                                        .attr('aria-hidden', 'true')
                                                        .html('&raquo;')
                                                )
                                                .append(
                                                    AJS.$('<span/>')
                                                        .addClass('sr-only')
                                                        .text(AJS.I18n.getText('status.report.next'))
                                                )
                                        )
                                )
                        )
                );

        }

        /**
         *
         * @param tableBody
         */
        function searchTable(tableBody) {
            AJS.$("#search").on("keyup", function () {
                AJS.$.each(tableBody.find("tr"), function () {

                    var text = AJS.$(this)
                        .text()
                        .replace(/ /g, '')
                        .replace(/(\r\n|\n|\r)/gm, "");

                    var searchTerm = AJS.$("#search").val();

                    if (text.toLowerCase().indexOf(searchTerm.toLowerCase()) == -1) {
                        AJS.$(this)
                            .hide()
                            .removeClass('search-item');
                    } else {
                        AJS.$(this)
                            .show()
                            .addClass('search-item');
                    }

                    if (searchTerm != '') {
                        activeSearch = true;
                    } else {
                        activeSearch = false;
                    }
                });

                setPageCount(tableBody);
                addPages();
                paginate(tableBody, 1);
            });
        }

        /**
         *
         * @param tableBody
         */
        function setPageCount(tableBody) {
            if (activeSearch) {
                pageCount = Math.ceil(tableBody.children('.search-item').length / pageRowCount);
            } else {
                pageCount = Math.ceil(tableBody.children('tr').length / pageRowCount);
            }

            if (pageCount == 0) {
                pageCount = 1;
            }
        }

        /**
         *
         * @param tableBody
         * @param page
         */
        function paginate(tableBody, page) {
            if (page == 'next') {
                page = actualPage + 1;
            } else if (page == 'previous') {
                page = actualPage - 1;
            }

            actualPage = page;

            var rows = (activeSearch ? tableBody.find(".search-item") : tableBody.find("tr"));
            var endRow = (pageRowCount * page);
            var startRow = (endRow - pageRowCount);
            var pagination = AJS.$('.pagination');

            rows
                .hide();

            rows
                .slice(startRow, endRow)
                .show();

            pagination
                .find('li')
                .removeClass('active disabled');

            pagination
                .find('li:eq(' + page + ')')
                .addClass('active');

            if (page == 1) {
                pagination
                    .find('li:first')
                    .addClass('disabled');

            } else if (page == pageCount) {
                pagination
                    .find('li:last')
                    .addClass('disabled');
            }
        }
    }
}(jQuery));