package com.unlimax.jira.plugin.workflow;

import com.atlassian.jira.user.UserUtils;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.worklog.WorkRatio;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.ObjectUtils;
import com.atlassian.jira.workflow.WorkflowActionsBean;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class WorkflowUtils
{
  public static final String SPLITTER = "@@";
  private static final WorkflowActionsBean workflowActionsBean = new WorkflowActionsBean();
  public static final String CASCADING_SELECT_TYPE = "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect";
  private static final Logger log = Logger.getLogger(WorkflowUtils.class);

  public static String getFieldNameFromKey(String key)
  {
    return getFieldFromKey(key).getName();
  }

  public static Field getFieldFromKey(String key)
  {
    FieldManager fieldManager = ComponentAccessor.getFieldManager();
    Field field;
    if (fieldManager.isCustomField(key))
      field = fieldManager.getCustomField(key);
    else {
      field = fieldManager.getField(key);
    }

    if (field == null) {
      throw new IllegalArgumentException("Unable to find field '" + key + "'");
    }

    return field;
  }

  public static Object getFieldValueFromIssue(Issue issue, Field field)
  {
    FieldManager fldManager = ComponentAccessor.getFieldManager();
    Object retVal = null;
    try
    {
      if (fldManager.isCustomField(field))
      {
        CustomField customField = (CustomField)field;
        Object value = issue.getCustomFieldValue(customField);

        if ("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect".equals(customField.getCustomFieldType().getKey())) {
          CustomFieldParams params = (CustomFieldParams)value;

          if (params != null) {
            Option parent = (Option)params.getFirstValueForNullKey();
            Option child = (Option)params.getFirstValueForKey("1");

            if (parent != null)
              if (ObjectUtils.isValueSelected(child)) {
                retVal = child.toString();
              } else {
                List childOptions = parent.getChildOptions();

                if ((childOptions == null) || (childOptions.isEmpty()))
                  retVal = parent.toString();
              }
          }
        }
        else
        {
          retVal = value;
        }

        if (log.isDebugEnabled()) {
          log.debug(
            String.format(
            "Got field value [object=%s;class=%s]", new Object[] { 
            retVal, retVal != null ? retVal.getClass() : "" }));
        }
      }
      else
      {
        String fieldId = field.getId();
        Collection retCollection = null;

        if (fieldId.equals("attachment"))
        {
          retCollection = issue.getAttachments();

          if ((retCollection != null) && (!retCollection.isEmpty()))
            retVal = retCollection;
        }
        else if (fieldId.equals("versions")) {
          retCollection = issue.getAffectedVersions();

          if ((retCollection != null) && (!retCollection.isEmpty()))
            retVal = retCollection;
        }
        else if (fieldId.equals("comment"))
        {
          try {
            retCollection = ComponentAccessor.getIssueManager().getEntitiesByIssueObject(
              "IssueComments", issue);

            if ((retCollection == null) || (retCollection.isEmpty())) return retVal;
              retVal = retCollection;
          }
          catch (GenericEntityException e) {
            retVal = null;
          }
        } else if (fieldId.equals("components")) {
          retCollection = issue.getComponents();

          if ((retCollection != null) && (!retCollection.isEmpty()))
            retVal = retCollection;
        }
        else if (fieldId.equals("fixVersions")) {
          retCollection = issue.getFixVersions();

          if ((retCollection != null) && (!retCollection.isEmpty()))
            retVal = retCollection;
        }
        else if (!fieldId.equals("thumbnail"))
        {
          if (fieldId.equals("issuetype"))
            retVal = issue.getIssueType();
          else if (!fieldId.equals("timetracking"))
          {
            if (fieldId.equals("issuelinks")) {
              retVal = ComponentAccessor.getIssueLinkManager().getIssueLinks(issue.getId());
            } else if (fieldId.equals("workratio")) {
              retVal = String.valueOf(WorkRatio.getWorkRatio(issue));
            } else if (fieldId.equals("issuekey")) {
              retVal = issue.getKey();
            } else if (fieldId.equals("subtasks")) {
              retCollection = issue.getSubTaskObjects();

              if ((retCollection != null) && (!retCollection.isEmpty()))
                retVal = retCollection;
            }
            else if (fieldId.equals("priority")) {
              retVal = issue.getPriority();
            } else if (fieldId.equals("resolution")) {
              retVal = issue.getResolution();
            } else if (fieldId.equals("status")) {
              retVal = issue.getStatus();
            } else if (fieldId.equals("project")) {
              retVal = issue.getProjectObject();
            } else if (fieldId.equals("security")) {
              retVal = issue.getSecurityLevel();
            } else if (fieldId.equals("timeestimate")) {
              retVal = issue.getEstimate();
            } else if (fieldId.equals("timespent")) {
              retVal = issue.getTimeSpent();
            } else if (fieldId.equals("assignee")) {
              retVal = issue.getAssignee();
            } else if (fieldId.equals("reporter")) {
              retVal = issue.getReporter();
            } else if (fieldId.equals("description")) {
              retVal = issue.getDescription();
            } else if (fieldId.equals("environment")) {
              retVal = issue.getEnvironment();
            } else if (fieldId.equals("summary")) {
              retVal = issue.getSummary();
            } else if (fieldId.equals("duedate")) {
              retVal = issue.getDueDate();
            } else if (fieldId.equals("updated")) {
              retVal = issue.getUpdated();
            } else if (fieldId.equals("created")) {
              retVal = issue.getCreated();
            } else if (fieldId.equals("resolutiondate")) {
              retVal = issue.getResolutionDate();
            } else {
              log.warn("Issue field \"" + fieldId + "\" is not supported.");

              GenericValue gvIssue = issue.getGenericValue();

              if (gvIssue != null)
                retVal = gvIssue.get(fieldId); 
            }
          }
        }
      }
    } catch (NullPointerException e) {
      retVal = null;

      log.error("Unable to get field \"" + field.getId() + "\" value", e);
    }

    return retVal;
  }

  public static void setFieldValue(MutableIssue issue, Field field, Object value, IssueChangeHolder changeHolder)
  {
    FieldManager fldManager = ComponentAccessor.getFieldManager();
    CustomFieldParams fieldParams;
    if (fldManager.isCustomField(field)) {
      CustomField customField = (CustomField)field;
      Object oldValue = issue.getCustomFieldValue(customField);

      CustomFieldType cfType = customField.getCustomFieldType();
        FieldLayoutItem fieldLayoutItem;

      if (log.isDebugEnabled()) {
        log.debug(
          String.format(
          "Set custom field value [field=%s,type=%s,oldValue=%s,newValueClass=%s,newValue=%s]", new Object[] { 
          customField, 
          cfType, 
          oldValue, 
          value != null ? value.getClass().getName() : "null", 
          value }));
      }

      try
      {
        fieldLayoutItem = getFieldLayoutItem(issue, field);
      }
      catch (FieldLayoutStorageException e)
      {
        log.error("Unable to get field layout item", e);
        throw new IllegalStateException(e);
      }
        Object newValue = value;

      if ((value instanceof IssueConstant)) {
        newValue = ((IssueConstant)value).getName();
      } else if ((value instanceof User)) {
        newValue = ((User)value).getName();
      } else if ((value instanceof GenericValue)) {
        GenericValue gv = (GenericValue)value;

        if ("SchemeIssueSecurityLevels".equals(gv.getEntityName())) {
          newValue = gv.getString("name");
        }
      }

      if ((newValue instanceof String))
      {
          fieldParams = new CustomFieldParamsImpl(customField, newValue);

        newValue = cfType.getValueFromCustomFieldParams(fieldParams);
      } else if (((newValue instanceof Collection)) &&
        (!(customField.getCustomFieldType() instanceof AbstractMultiCFType)))
      {
        fieldParams = new CustomFieldParamsImpl(
          customField, 
          StringUtils.join((Collection)newValue, ","));

        newValue = cfType.getValueFromCustomFieldParams(fieldParams);
      }

      if (log.isDebugEnabled()) {
        log.debug("Got new value [class=" + (
          newValue != null ? newValue.getClass().getName() : "null") + 
          ",value=" + 
          newValue + 
          "]");
      }

      issue.setCustomFieldValue(customField, newValue);

      customField.updateValue(
        fieldLayoutItem, issue, 
        new ModifiedValue(oldValue, newValue), changeHolder);

      if (log.isDebugEnabled()) {
        log.debug(
          "Issue [" + 
          issue + 
          "] got modfied fields - [" + 
          issue.getModifiedFields() + 
          "]");
      }

      if (issue.getKey() != null)
      {
        if (issue.getModifiedFields().containsKey(field.getId()))
          issue.getModifiedFields().remove(field.getId());
      }
    }
    else {
      String fieldId = field.getId();

      if (fieldId.equals("attachment")) {
        throw new UnsupportedOperationException("Not implemented");
      }

      if (fieldId.equals("versions")) {
        if (value == null) {
            Collection<Version> vs = new ArrayList<Version>();
            issue.setAffectedVersions(vs);
        } else if ((value instanceof String)) {
          VersionManager versionManager = ComponentAccessor.getVersionManager();
          Version v = versionManager.getVersion(issue.getProjectObject().getId(), (String)value);

          if (v != null)
            issue.setAffectedVersions(Arrays.asList(new Version[] { v }));
          else
            throw new IllegalArgumentException("Wrong affected version value");
        }
        else if ((value instanceof Version)) {
          issue.setAffectedVersions(Arrays.asList(new Version[] { (Version)value }));
        } else if ((value instanceof Collection)) {
          issue.setAffectedVersions((Collection)value);
        } else {
          throw new IllegalArgumentException("Wrong affected version value");
        }
      } else {
        if (fieldId.equals("comment")) {
          throw new UnsupportedOperationException("Not implemented");
        }

        if (fieldId.equals("fixVersions")) {
          if (value == null) {
              Collection<Version> fvs = new ArrayList<Version>();
            issue.setFixVersions(fvs);
          } else if ((value instanceof String)) {
            VersionManager versionManager = ComponentAccessor.getVersionManager();
            Version v = versionManager.getVersion(issue.getProjectObject().getId(), (String)value);

            if (v != null)
              issue.setFixVersions(Arrays.asList(new Version[] { v }));
          }
          else if ((value instanceof Version)) {
            issue.setFixVersions(Arrays.asList(new Version[] { (Version)value }));
          } else if ((value instanceof Collection)) {
            issue.setFixVersions((Collection)value);
          } else {
            throw new IllegalArgumentException("Wrong fix version value");
          }
        } else {
          if (fieldId.equals("thumbnail")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("issuetype")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("timetracking")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("issuelinks")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("workratio")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("issuekey")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("subtasks")) {
            throw new UnsupportedOperationException("Not implemented");
          }

          if (fieldId.equals("priority")) {
            if (value == null)
              issue.setPriority(null);
            else
              throw new UnsupportedOperationException("Not implemented");
          }
          if (fieldId.equals("security")) {
            if (value == null) {
              issue.setSecurityLevel(null);
            } else if ((value instanceof GenericValue)) {
              issue.setSecurityLevel((GenericValue)value);
            } else if ((value instanceof Long)) {
              issue.setSecurityLevelId((Long)value);
            } else if ((value instanceof String))
            {
                Collection levels;
              try
              {

                 levels = ComponentAccessor.getIssueSecurityLevelManager().getSecurityLevelsByName((String)value);
              }
              catch (GenericEntityException e)
              {
                throw new IllegalArgumentException("Unable to find security level \"" + value + "\"");
              }

              if (levels == null) {
                throw new IllegalArgumentException("Unable to find security level \"" + value + "\"");
              }

              if (levels.size() > 1) {
                throw new IllegalArgumentException("More that one security level with name \"" + value + "\"");
              }

              issue.setSecurityLevel((GenericValue)levels.iterator().next());
            } else {
              throw new UnsupportedOperationException("Not implemented");
            }
          }
          else if (fieldId.equals("duedate")) {
            if (value == null) {
              issue.setDueDate(null);
            }

            if ((value instanceof Timestamp)) {
              issue.setDueDate((Timestamp)value);
            } else if ((value instanceof String)) {
              ApplicationProperties properties = ComponentAccessor.getApplicationProperties();
              SimpleDateFormat formatter = new SimpleDateFormat(
                properties.getDefaultString("jira.date.time.picker.java.format"));
              try
              {
                Date date = formatter.parse((String)value);

                if (date != null) {
                  issue.setDueDate(new Timestamp(date.getTime())); return;
                }
                issue.setDueDate(null);
              }
              catch (ParseException e) {
                throw new IllegalArgumentException("Wrong date format exception for \"" + value + "\"");
              }
            }
          }
          else if (fieldId.equals("summary")) {
            if ((value == null) || ((value instanceof String)))
              issue.setSummary((String)value);
            else
              throw new UnsupportedOperationException("Wrong value type for setting 'Summary'");
          }
          else if (fieldId.equals("description")) {
            if ((value == null) || ((value instanceof String)))
              issue.setDescription((String)value);
            else
              throw new UnsupportedOperationException("Wrong value type for setting 'Description'");
          }
          else if (fieldId.equals("environment")) {
            if ((value == null) || ((value instanceof String)))
              issue.setEnvironment((String)value);
            else
              throw new UnsupportedOperationException("Wrong value type for setting 'Environment'");
          }
          else {
            log.error("Issue field \"" + fieldId + "\" is not supported for setting.");
          }
        }
      }
    }
  }

  public static void setFieldValue(MutableIssue issue, String fieldKey, Object value, IssueChangeHolder changeHolder)
  {
    Field field = getFieldFromKey(fieldKey);

    setFieldValue(issue, field, value, changeHolder);
  }

  public static List<Group> getGroups(String strGroups, String splitter)
  {
    String[] groups = strGroups.split("\\Q" + splitter + "\\E");
    List groupList = new ArrayList(groups.length);

    for (String s : groups) {
      Group group = ComponentAccessor.getGroupManager().getGroup(s);

      groupList.add(group);
    }

    return groupList;
  }

  public static String getStringGroup(Collection<Group> groups, String splitter)
  {
    StringBuilder sb = new StringBuilder();

    for (Group g : groups) {
      sb.append(g.getName()).append(splitter);
    }

    return sb.toString();
  }

  public static List<Field> getFields(String strFields, String splitter)
  {
    String[] fields = strFields.split("\\Q" + splitter + "\\E");
    List fieldList = new ArrayList(fields.length);

    for (String s : fields) {
      Field field = ComponentAccessor.getFieldManager().getField(s);

      if (field != null) {
        fieldList.add(field);
      }
    }

    return fieldList;
  }

  public static String getStringField(Collection<Field> fields, String splitter)
  {
    StringBuilder sb = new StringBuilder();

    for (Field f : fields) {
      sb.append(f.getId()).append(splitter);
    }

    return sb.toString();
  }

  public static FieldScreen getFieldScreen(ActionDescriptor actionDescriptor)
  {
    return workflowActionsBean.getFieldScreenForView(actionDescriptor);
  }

  public static FieldLayoutItem getFieldLayoutItem(Issue issue, Field field)
    throws FieldLayoutStorageException
  {
    FieldLayoutManager fieldLayoutManager = ComponentAccessor.getFieldLayoutManager();
    FieldLayout layout = fieldLayoutManager.getFieldLayout(issue.getProjectObject(), issue.getIssueType().getId());

    if (layout.getId() == null) {
      layout = fieldLayoutManager.getEditableDefaultFieldLayout();
    }

    return layout.getFieldLayoutItem(field.getId());
  }
}