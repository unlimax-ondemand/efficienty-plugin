package com.unlimax.jira.plugin.workflow;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkflowCalculateTimeSpentFunctionPluginFactory extends AbstractWorkflowPluginFactory
  implements WorkflowPluginFunctionFactory
{
  public static final String PARAM_FIELD_ID = "fieldId";
  public static final String TARGET_FIELD_NAME = "field.name";
  private final CustomFieldManager customFieldManager;

  public WorkflowCalculateTimeSpentFunctionPluginFactory(CustomFieldManager customFieldManager)
  {
    this.customFieldManager = customFieldManager;
  }

  public Map getDescriptorParams(Map conditionParams)
  {
    Map params = new HashMap();

    String fieldId = extractSingleParam(conditionParams, "fieldId");
    params.put("field.name", fieldId);

    return params;
  }

  protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor)
  {
    getVelocityParamsForInput(velocityParams);
    if (!(descriptor instanceof FunctionDescriptor)) {
      throw new IllegalArgumentException(
        "Descriptor must be a FunctionDescriptor.");
    }
    FunctionDescriptor functionDescriptor = (FunctionDescriptor)descriptor;

    velocityParams.put("fieldId", functionDescriptor.getArgs()
      .get("field.name"));
  }

  protected void getVelocityParamsForInput(Map velocityParams)
  {
    List customFields = this.customFieldManager
      .getCustomFieldObjects();

    velocityParams.put("fields", customFields);
  }

  protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor)
  {
    if (!(descriptor instanceof FunctionDescriptor)) {
      throw new IllegalArgumentException(
        "Descriptor must be a FunctionDescriptor.");
    }
    FunctionDescriptor functionDescriptor = (FunctionDescriptor)descriptor;

    String fieldName = (String)functionDescriptor.getArgs().get(
      "field.name");

    velocityParams.put("fieldId", 
      this.customFieldManager.getCustomFieldObject(fieldName).getNameKey());
  }
}