package com.unlimax.jira.plugin.report;

public class UserIssueSpentEntity
{
  private String userName;
  private long timeSpentBySeconds;
  private long timeSpentByHours;
  private int issueCount;

  public UserIssueSpentEntity(String userName, long timeSpentBySeconds, long timeSpentByHours, int issueCount)
  {
    this.userName = userName;
    this.timeSpentBySeconds = timeSpentBySeconds;
    this.timeSpentByHours = timeSpentByHours;
    this.issueCount = issueCount;
  }

  public String getUserName() {
    return this.userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public int getIssueCount() {
    return this.issueCount;
  }

  public void setIssueCount(int issueCount) {
    this.issueCount = issueCount;
  }

  public long getTimeSpentByHours() {
    return this.timeSpentByHours;
  }

  public void setTimeSpentByHours(long timeSpentByHours) {
    this.timeSpentByHours = timeSpentByHours;
  }

  public void setTimeSpentBySeconds(long timeSpentBySeconds)
  {
    this.timeSpentBySeconds = timeSpentBySeconds;
  }

  public long getTimeSpentBySeconds() {
    return this.timeSpentBySeconds;
  }

  public long getAverageBySeconds() {
    if (this.issueCount == 0) return 0L;
    return this.timeSpentBySeconds / this.issueCount;
  }

  public long getAverageByHours() {
    if (this.issueCount == 0) return 0L;
    return this.timeSpentBySeconds / 3600L / this.issueCount;
  }

  public void addCounts(UserIssueSpentEntity uise)
  {
    this.issueCount += uise.getIssueCount();
    this.timeSpentBySeconds += uise.getTimeSpentBySeconds();
    this.timeSpentByHours = (this.timeSpentBySeconds / 3600L);
  }

  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserIssueSpentEntity other = (UserIssueSpentEntity)obj;
    if (this.userName == null) {
      if (other.userName != null)
        return false;
    } else if (!this.userName.equals(other.userName))
      return false;
    return true;
  }
}