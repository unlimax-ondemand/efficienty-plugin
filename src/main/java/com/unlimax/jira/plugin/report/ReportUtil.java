package com.unlimax.jira.plugin.report;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import java.sql.Timestamp;
import java.util.*;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.order.SortOrder;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import com.atlassian.jira.component.ComponentAccessor;

public class ReportUtil
{
  public static long getIssueTimeSpentBySeconds(Issue issue)
  {
    OfBizDelegator delegator = ComponentAccessor.getOfBizDelegator();
    Map params = EasyMap.build("issue", issue.getId());
    List changeGroups = delegator.findByAnd("ChangeGroup", params);
    Timestamp ts = null;

    Collections.sort(changeGroups, new Comparator() {
      public int compare(GenericValue o1, GenericValue o2) {
        return o1.getTimestamp("created").compareTo(
          o2.getTimestamp("created"));
      }

      public int compare(Object x0, Object x1) {
        return compare((GenericValue)x0, (GenericValue)x1);
      }
    });
    for (Iterator i = changeGroups.iterator(); i.hasNext(); ) {
      GenericValue changeGroup = (GenericValue)i.next();
      Map paramsItem = EasyMap.build("group", changeGroup.getLong("id"), 
        "field", "status", "fieldtype", "jira");
      List changeItems = delegator.findByAnd("ChangeItem", paramsItem);
      if (changeItems.size() > 0)
        ts = changeGroup.getTimestamp("created");
    }
    if (ts == null)
      return 0L;
    return (ts.getTime() - issue.getCreated().getTime()) / 1000L;
  }

  public static List<UserIssueSpentEntity> getIssueTimeSpentByUsers(Issue issue)
  {
    return getIssueTimeSpentByUsers(
      issue.getId().longValue(), new Timestamp(issue.getCreated().getTime()));
  }

  public static List<UserIssueSpentEntity> getIssueTimeSpentByUsers(long issueId, Timestamp issueeCreateDate)
  {
    List uiseList = new ArrayList();

    OfBizDelegator delegator =ComponentAccessor.getOfBizDelegator();

    Map params = EasyMap.build("issue", Long.valueOf(issueId));
    List changeGroups = delegator.findByAnd("ChangeGroup", params);

    Collections.sort(changeGroups, new Comparator() {
      public int compare(GenericValue o1, GenericValue o2) {
        return o1.getTimestamp("created").compareTo(
          o2.getTimestamp("created"));
      }

      public int compare(Object x0, Object x1) {
        return compare((GenericValue)x0, (GenericValue)x1);
      }
    });
    Timestamp tsStartDate = issueeCreateDate;

    for (Iterator i = changeGroups.iterator(); i.hasNext(); ) {
      GenericValue changeGroup = (GenericValue)i.next();

      Map paramsItem = EasyMap.build("group", changeGroup.getLong("id"), 
        "field", "status", "fieldtype", "jira");
      List changeItems = delegator.findByAnd("ChangeItem", paramsItem);
      if (changeItems.size() > 0) {
        long duration = changeGroup.getTimestamp("created").getTime() - 
          tsStartDate.getTime();
        String user = changeGroup.getString("author");
        UserIssueSpentEntity uise = new UserIssueSpentEntity(user, 
          duration / 1000L, duration / 1000L / 3600L, 0);
        if (!uiseList.contains(uise))
          uise.setIssueCount(1);
        uiseList.add(uise);
        tsStartDate = new Timestamp(
          changeGroup.getTimestamp("created").getTime());
      }
      else
      {
        paramsItem = EasyMap.build("group", changeGroup.getLong("id"), 
          "field", "assignee", "fieldtype", "jira");
        changeItems = delegator.findByAnd("ChangeItem", paramsItem);
        if (changeItems.size() > 0) {
          GenericValue changeItem = (GenericValue)changeItems.get(0);
          long duration = changeGroup.getTimestamp("created").getTime() - 
            tsStartDate.getTime();
          String user = changeItem.getString("oldvalue");
          UserIssueSpentEntity uise = new UserIssueSpentEntity(user, 
            duration / 1000L, duration / 1000L / 3600L, 0);
          if (!uiseList.contains(uise))
            uise.setIssueCount(1);
          uiseList.add(uise);
          tsStartDate = new Timestamp(
            changeGroup.getTimestamp("created").getTime());
        }
      }
    }
    return uiseList;
  }

  public static String getDurationString(long inseconds)
  {
    String retVal = "";
    long duration = inseconds;

    if (duration != 0L) {
      long days = duration / 86400L;
      long restDay = duration % 86400L;

      long hours = restDay / 3600L;
      long resthours = restDay % 3600L;

      long minutes = resthours / 60L;
      long seconds = resthours % 60L;

      retVal = 
        String.valueOf(days) + "d ";
      retVal = retVal + (
        hours == 0L ? "" : 
        new StringBuilder().append(String.valueOf(hours)).append("h ").toString());

      retVal = retVal + (
        minutes == 0L ? "" : 
        new StringBuilder().append(String.valueOf(minutes)).append("m ").toString());

      if ((days == 0L) && (hours == 0L))
        retVal = retVal + (
          seconds == 0L ? "" : 
          new StringBuilder().append(String.valueOf(seconds))
          .append("s").toString());
    } else {
      retVal = "0s";
    }
    return retVal;
  }

  public static Map<String,String> getFilterInfo(JiraServiceContext ctx, Map params) {
    ProjectManager projectManager = ComponentAccessor.getProjectManager();
    SearchRequestService searchRequestService = ComponentAccessor.getComponent(SearchRequestService.class);
    Map<String,String> filterInfo = new HashMap<String, String>();
    String strFilterId = params.get("filterId").toString();
    String strSelectedProjectId = params.get("selectedProjectId").toString();
    Long filterBy = 0l;
    if (!StringUtils.isEmpty(strFilterId)) {
      if (strFilterId.split("-")[0].equals("project")) {
        filterInfo.put("filterType","project");
        filterInfo.put("filterName", projectManager.getProjectObj(Long.parseLong(strFilterId.split("-")[1])).getName());
        filterInfo.put("filterId",projectManager.getProjectObj(Long.parseLong(strFilterId.split("-")[1])).getId().toString());
      } else{
        filterInfo.put("filterName", searchRequestService.getFilter(ctx, Long.parseLong(strFilterId.split("-")[1])).getName());
        filterInfo.put("filterId",searchRequestService.getFilter(ctx,Long.parseLong(strFilterId.split("-")[1])).getId().toString());
        filterInfo.put("filterType","filter");
      }
    }else if (!StringUtils.isEmpty(strSelectedProjectId)) {
      filterInfo.put("filterType","project");
      filterInfo.put("filterName",projectManager.getProjectObj(Long.parseLong(strSelectedProjectId)).getName());
      filterInfo.put("filterId",projectManager.getProjectObj(Long.parseLong(strSelectedProjectId)).getId().toString());
    }
    return filterInfo;
  }

  //根据搜索条件获取数据
  public static SearchResults getSearchResult(Map params) throws SearchException {
    String strFilterId = params.get("filterId").toString();
    String strSelectedProjectId = params.get("selectedProjectId").toString();
    SearchResults results = null;

    if(!StringUtils.isEmpty(strFilterId)) {
      if (strFilterId.split("-")[0].equals("project")) {
        Long selectedProjectId = Long.parseLong(strFilterId.split("-")[1]);
        results = getSearchResultByProject(selectedProjectId);
      } else {
        Long filterId = Long.parseLong(strFilterId.split("-")[1]);
        results = getSearchResultByFilter(filterId);
      }

    }else if(!StringUtils.isEmpty(strSelectedProjectId)) {
      Long selectedProjectId = Long.parseLong(params.get("selectedProjectId").toString());
      results = getSearchResultByProject(selectedProjectId);
    }

    if(results==null){
      return null;
    }

    return results;
  }

  //根据选择的项目获取搜索条件
  public static SearchResults getSearchResultByProject(Long projectId) throws SearchException{
    SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
    ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();

    builder.where().project(projectId).endWhere().orderBy().issueId(SortOrder.ASC);
    Query query = builder.buildQuery();
    SearchResults results = searchService.search(currentUser, query, PagerFilter.getUnlimitedFilter());
    return results;

  }

  //根据选择的过滤器获取搜索条件
  public static SearchResults getSearchResultByFilter(Long filterId) throws SearchException {
    SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
    SearchRequestService searchRequestService = ComponentAccessor.getComponent(SearchRequestService.class);
    ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    JiraServiceContext ctx = new JiraServiceContextImpl(currentUser);

    SearchRequest searchRequest = searchRequestService.getFilter(ctx, new Long(filterId));

    //only for JIRA 6.x
    // SearchResults results= searchProvider.search(searchRequest.getQuery(), currentUser, PagerFilter.getUnlimitedFilter());
    SearchResults results= searchService.search(currentUser,searchRequest.getQuery(),PagerFilter.getUnlimitedFilter());
    return results;

  }



}