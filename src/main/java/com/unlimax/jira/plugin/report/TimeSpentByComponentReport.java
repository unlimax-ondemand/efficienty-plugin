package com.unlimax.jira.plugin.report;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

public class TimeSpentByComponentReport extends AbstractReport
{
  private final SearchService searchService;
  private final ProjectManager projectManager;

  public TimeSpentByComponentReport(SearchService searchService, ProjectManager projectManager)
  {
    this.searchService = searchService;
    this.projectManager = projectManager;
  }

  public boolean isExcelViewSupported() {
    return true;
  }

  public String generateReportHtml(ProjectActionSupport action, Map params)
    throws Exception
  {
    return generateReport(action, params, false);
  }

  public String generateReportExcel(ProjectActionSupport action, Map params)
    throws Exception
  {
    final StringBuilder contentDispositionValue = new StringBuilder(50);

    contentDispositionValue.append("attachment;filename=\"");
    contentDispositionValue.append(getDescriptor().getName()). append(".xls\";");
    final HttpServletResponse response = ActionContext.getResponse();
    response.addHeader("content-disposition", contentDispositionValue. toString());

    return generateReport(action, params, true);
  }

  public String generateReport(ProjectActionSupport action, Map params, boolean excelView)
    throws Exception
  {
    ApplicationUser remoteUser = action.getLoggedInUser();
    List retList = new ArrayList();
    Map velocityParams = new HashMap();
    String viewName = "view";

    SimpleDateFormat sdf  =   new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    Date date = new Date();

    Long projectId = ParameterUtils.getLongParam(params, "selectedProjectId");
    Project project = this.projectManager.getProjectObj(projectId);

    Collection<ProjectComponent> pcs = project.getProjectComponents();
    if (pcs.size() == 0) {
      velocityParams.put("errorcode", "No data found");
      return this.descriptor.getHtml("error", velocityParams);
    }

    for (ProjectComponent pc : pcs) {
      JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();

      Query query = queryBuilder.where().project(new Long[] { projectId }).and()
        .component(new Long[] { 
        pc.getId() }).buildQuery();
      SearchResults sr = searchService.search(remoteUser,query,
        PagerFilter.getUnlimitedFilter());

      List<Issue> issues = sr.getIssues();

      long total = 0L;
      for (Issue issue : issues) {
        total += ReportUtil.getIssueTimeSpentBySeconds(issue);
      }

      TimeSpentEntity tse = new TimeSpentEntity();
      tse.setName(pc.getName());
      tse.setComponent(pc);
      tse.setIssueCount(issues.size());
      tse.setTimeSpentBySeconds(total);
      tse.setTimeSpentByHours(total / 3600L);
      retList.add(tse);
    }

    Collections.sort(retList, new Comparator() {
      public int compare(TimeSpentEntity o1, TimeSpentEntity o2) {
        return Long.valueOf(o1.getAverageBySeconds()).compareTo(
          Long.valueOf(o2.getAverageBySeconds()));
      }
      public int compare(Object x0, Object x1) {
        return compare((TimeSpentEntity)x0, (TimeSpentEntity)x1);
      }
    });
    velocityParams.put("extractedDate",sdf.format(date));
    velocityParams.put("compomentTimeSpentList", retList);
    velocityParams.put("projectName",
            ComponentAccessor.getProjectManager().getProjectObj(projectId).getName());
    velocityParams.put("projectKey",
            ComponentAccessor.getProjectManager().getProjectObj(projectId).getKey());
    velocityParams.put("util", new ReportUtil());

    if (excelView) {
      viewName = "excelView";
    }
    return this.descriptor.getHtml(viewName, velocityParams);
  }
}