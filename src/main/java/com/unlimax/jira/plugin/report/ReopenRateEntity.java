package com.unlimax.jira.plugin.report;

import com.atlassian.crowd.embedded.api.User;

public class ReopenRateEntity
{
  private User user = null;
  private int resovledCount = 0;
  private int reopenCount = 0;
  private int issueCount = 0;
  private String userName;

  public void setUser(User user)
  {
    this.user = user;
  }

  public User getUser() {
    return this.user;
  }

  public void setResovledCount(int resovledCount)
  {
    this.resovledCount = resovledCount;
  }
  public int getResovledCount() {
    return this.resovledCount;
  }
  public void setReopenCount(int reopeningCount) {
    this.reopenCount = reopeningCount;
  }
  public int getReopenCount() {
    return this.reopenCount;
  }
  public void setIssueCount(int issueCount) {
    this.issueCount = issueCount;
  }
  public int getIssueCount() {
    return this.issueCount;
  }
  public int addReopenCount(int addvalue) {
    this.reopenCount += addvalue;
    return this.reopenCount;
  }
  public int addResovledCount(int addvalue) {
    this.resovledCount += addvalue;
    return this.resovledCount;
  }

  public int addIssueCount(int addvalue) {
    this.issueCount += addvalue;
    return this.issueCount;
  }
  public void addCounts(ReopenRateEntity inObj) {
    addIssueCount(inObj.getIssueCount());
    addReopenCount(inObj.getReopenCount());
    addResovledCount(inObj.getResovledCount());
  }

  public boolean equals(Object obj) {
    ReopenRateEntity inObj = (ReopenRateEntity)obj;
    return inObj.getUserName().equals(getUserName());
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return this.userName;
  }
}