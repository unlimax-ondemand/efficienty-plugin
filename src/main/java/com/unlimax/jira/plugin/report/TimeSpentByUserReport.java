package com.unlimax.jira.plugin.report;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

public class TimeSpentByUserReport extends AbstractReport
{
  private final IssueManager issueManager;

  public TimeSpentByUserReport(IssueManager issueManager)
  {
    this.issueManager = issueManager;
  }

  public boolean isExcelViewSupported() {
    return true;
  }

  public String generateReportHtml(ProjectActionSupport action, Map params)
    throws Exception
  {
    return generateReport(action, params, false);
  }

  @Override
  public String generateReportExcel(ProjectActionSupport action, Map reqParams) throws Exception {

    final StringBuilder contentDispositionValue = new StringBuilder(50);

    contentDispositionValue.append("attachment;filename=\"");
    contentDispositionValue.append(getDescriptor().getName()). append(".xls\";");
    final HttpServletResponse response = ActionContext.getResponse();
    response.addHeader("content-disposition", contentDispositionValue. toString());

    return generateReport(action, reqParams, true);

  }

  public String generateReport(ProjectActionSupport action, Map params, boolean excelView)
    throws Exception
  {
    List retList = new ArrayList();

    String viewName = "view";

      SimpleDateFormat sdf  =   new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
      Date date = new Date();

      ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
      JiraServiceContext ctx = new JiraServiceContextImpl(currentUser);

      SearchResults searchResults = ReportUtil.getSearchResult(params);

      Collection<Issue> issues = searchResults.getIssues();
    for (Issue is:issues){
        List<UserIssueSpentEntity> ls = ReportUtil.getIssueTimeSpentByUsers(is);
      for (UserIssueSpentEntity uise : ls) {
        if (retList.contains(uise)) {
          int index = retList.indexOf(uise);
          UserIssueSpentEntity cuise = (UserIssueSpentEntity)retList.get(index);
          cuise.addCounts(uise);
          retList.set(index, cuise);
        } else {
          retList.add(uise);
        }
      }
    }

    Collections.sort(retList, new Comparator() {
      public int compare(UserIssueSpentEntity o1, UserIssueSpentEntity o2) {
        return Long.valueOf(o1.getAverageBySeconds()).compareTo(
          Long.valueOf(o2.getAverageBySeconds()));
      }
      public int compare(Object x0, Object x1) {
        return compare((UserIssueSpentEntity)x0, (UserIssueSpentEntity)x1);
      }
    });

    Map velocityParams = new HashMap();
      velocityParams.put("extractedDate",sdf.format(date));
    velocityParams.put("userTimeSpentList",retList);
      velocityParams.put("filterInfo",ReportUtil.getFilterInfo(ctx, params));
      velocityParams.put("filterUrl","");
    velocityParams.put("util", new ReportUtil());

    if (excelView) {
      viewName = "excelView";
    }
    return this.descriptor.getHtml(viewName, velocityParams);
  }

  public Collection getIssueIdsByFilter(Long filterId) throws SearchException {
    SearchRequestService searchRequestService = ComponentAccessor.getComponent(SearchRequestService.class);
    SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
    List<Long> issueIds = new ArrayList<Long>();

    ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    JiraServiceContext ctx = new JiraServiceContextImpl(currentUser);

    SearchRequest searchRequest = searchRequestService.getFilter(ctx, new Long(filterId));

    //only for JIRA 6.x
    // SearchResults results= searchProvider.search(searchRequest.getQuery(), currentUser, PagerFilter.getUnlimitedFilter());
    SearchResults results= searchService.search(currentUser,searchRequest.getQuery(), PagerFilter.getUnlimitedFilter());

    List<Issue> issues = results.getIssues();
    if(issues.size()==0){
      return null;
    }
    for (Issue is:issues) {
      issueIds.add(is.getId());
    }
    Collection<Long> issueResults = issueIds;
    return issueResults;
  }
}