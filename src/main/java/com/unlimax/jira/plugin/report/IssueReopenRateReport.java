package com.unlimax.jira.plugin.report;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.crowd.embedded.api.User;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntity;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

public class IssueReopenRateReport extends AbstractReport
{
  private final IssueManager issueManager;
  private static final Logger logger = Logger.getLogger(IssueReopenRateReport.class);

  public IssueReopenRateReport(IssueManager issueManager)
  {
    this.issueManager = issueManager;
  }

  public boolean isExcelViewSupported()
  {
    return true;
  }

  public String generateReportHtml(ProjectActionSupport action, Map params)
    throws Exception
  {
    return generateReport(action, params, false);
  }

  public String generateReportExcel(ProjectActionSupport action, Map params)
    throws Exception
  {
    final StringBuilder contentDispositionValue = new StringBuilder(50);

    contentDispositionValue.append("attachment;filename=\"");
    contentDispositionValue.append(getDescriptor().getName()). append(".xls\";");
    final HttpServletResponse response = ActionContext.getResponse();
    response.addHeader("content-disposition", contentDispositionValue. toString());

    return generateReport(action, params, true);
  }

  public String generateReport(ProjectActionSupport action, Map params, boolean excelView)
    throws Exception
  {
      ApplicationUser remoteUser = action.getLoggedInUser();

    List retList = new ArrayList();

    String viewName = "view";

    SimpleDateFormat sdf  =   new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    Date date = new Date();

    ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    JiraServiceContext ctx = new JiraServiceContextImpl(currentUser);

    SearchResults searchResults = ReportUtil.getSearchResult(params);
    Collection<Issue> issues = searchResults.getIssues();
    for (Issue is:issues){
        List<ReopenRateEntity> ls = getReopeningRateByUser(is,currentUser);
      for (ReopenRateEntity rre : ls) {
        if (retList.contains(rre)) {
          int index = retList.indexOf(rre);
          ReopenRateEntity crre = (ReopenRateEntity)retList.get(index);
          crre.addCounts(rre);
          retList.set(index, crre);
        } else {
          retList.add(rre);
        }
      }
    }
    Collections.sort(retList, new Comparator() {
      public int compare(ReopenRateEntity o1, ReopenRateEntity o2) {
        return Integer.valueOf(o1.getResovledCount() != 0 ? o1.getReopenCount() * 100 / o1.getResovledCount() : 0).compareTo(
          Integer.valueOf(o2.getResovledCount() != 0 ? o2.getReopenCount() * 100 / o2.getResovledCount() : 0));
      }
      public int compare(Object x0, Object x1) {
        return compare((ReopenRateEntity)x0, (ReopenRateEntity)x1);
      }
    });
    Map velocityParams = new HashMap();
    velocityParams.put("reopenRateList", retList);
    velocityParams.put("extractedDate",sdf.format(date));
    velocityParams.put("filterInfo",ReportUtil.getFilterInfo(ctx, params));
    velocityParams.put("filterUrl","");
    velocityParams.put("util", new ReportUtil());
    if (excelView) {
      viewName = "excelView";
    }
    return this.descriptor.getHtml(viewName, velocityParams);
  }

  private static List<ReopenRateEntity> getReopeningRateByUser(Issue issue, ApplicationUser remoteUser)
  {
    ChangeHistoryManager changeHistoryManager =
            ComponentAccessor.getChangeHistoryManager();
    List reopenings = new ArrayList();
    try {
      List changes = changeHistoryManager.getChangeHistoriesForUser(
        issue, remoteUser);
      ApplicationUser resolvedUser = null;
      for (int i = 0; i < changes.size(); i++) {
        ChangeHistory ch = (ChangeHistory)changes.get(i);
        List changeItems = ch.getChangeItems();
        for (int j = 0; j < changeItems.size(); j++) {
          GenericEntity changeItem = 
            (GenericEntity)changeItems
            .get(j);
          String ciField = changeItem.containsKey("field") ? 
            changeItem.getString("field") : 
            "";
          if (!"resolution".equals(ciField))
            continue;
          Object newValue = changeItem.get("newvalue");
          Object oldValue = changeItem.get("oldvalue");
          if ((newValue == null) && (oldValue != null)) {
            ReopenRateEntity rre = new ReopenRateEntity();
            rre.setUser(resolvedUser.getDirectoryUser());
            rre.setUserName(resolvedUser.getName());
            rre.addReopenCount(1);
            if (!reopenings.contains(rre))
              rre.addIssueCount(1);
            reopenings.add(rre);
          }
          else if ((newValue != null) && (oldValue == null)) {
            resolvedUser =ch.getAuthorObject();
            ReopenRateEntity rre = new ReopenRateEntity();
            rre.setUser(resolvedUser.getDirectoryUser());
            rre.setUserName(resolvedUser.getName());
            rre.addResovledCount(1);
            if (!reopenings.contains(rre))
              rre.addIssueCount(1);
            reopenings.add(rre);
          }
        }
      }
    }
    catch (Exception e)
    {
      logger.error("exception while getting actions", e);
    }

    return reopenings;
  }
}