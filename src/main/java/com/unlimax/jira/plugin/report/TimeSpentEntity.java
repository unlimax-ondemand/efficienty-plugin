package com.unlimax.jira.plugin.report;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.version.Version;

public class TimeSpentEntity
{
  private String name;
  private int issueCount;
  private long timeSpentBySeconds;
  private long timeSpentByHours;
  private ProjectComponent component;
  private Version version;

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName() {
    return this.name;
  }
  public long getTimeSpentBySeconds() {
    return this.timeSpentBySeconds;
  }
  public void setTimeSpentBySeconds(long timeSpentBySeconds) {
    this.timeSpentBySeconds = timeSpentBySeconds;
  }
  public void setTimeSpentByHours(long timeSpentByHours) {
    this.timeSpentByHours = timeSpentByHours;
  }
  public long getTimeSpentByHours() {
    return this.timeSpentByHours;
  }
  public void setIssueCount(int issueCount) {
    this.issueCount = issueCount;
  }
  public int getIssueCount() {
    return this.issueCount;
  }
  public void setComponent(ProjectComponent component) {
    this.component = component;
  }
  public ProjectComponent getComponent() {
    return this.component;
  }
  public void setVersion(Version version) {
    this.version = version;
  }
  public Version getVersion() {
    return this.version;
  }
  public long getAverageBySeconds() {
    if (this.issueCount == 0) return 0L;
    return this.timeSpentBySeconds / this.issueCount;
  }
  public long getAverageByHours() {
    if (this.issueCount == 0) return 0L;
    return this.timeSpentBySeconds / 3600L / this.issueCount;
  }
}