package com.unlimax.jira.plugin.gadget;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v1.util.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import com.sun.jersey.core.impl.provider.entity.XMLJAXBElementProvider;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Path("/reopen-rate-by-user")
@AnonymousAllowed
@Produces({"application/json"})
public class ReopenRateResource
{
  private final JiraAuthenticationContext authenticationContext;
  private SearchService searchService;
  private ProjectManager projectManager;
  private static final Logger logger = Logger.getLogger(ReopenRateResource.class);

  public ReopenRateResource(JiraAuthenticationContext authenticationContext, SearchService searchService, ProjectManager projectManager)
  {
    this.authenticationContext = authenticationContext;
    this.searchService = searchService;
    this.projectManager = projectManager;
  }
  @GET
  @Path("/getReopenRateList")
  public Response getReopenRateList(@QueryParam("projectId") String projectIdString) throws Exception {
    Long projectId = Long.valueOf(projectIdString.substring("project-".length()));
    String projectName = this.projectManager.getProjectObj(projectId).getName();
    String projectKey = this.projectManager.getProjectObj(projectId).getKey();

    JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
    Query query = queryBuilder.where().project(new Long[] { projectId }).buildQuery();
    SearchResults sr = searchService.search(authenticationContext.getLoggedInUser(),query,
       PagerFilter.getUnlimitedFilter());
    List<Issue> issues = sr.getIssues();

    List reopenRateList = new ArrayList();

    for (Issue issue : issues) {
        List<ReopenRateInfo> ls = getReopeningRateByUser(issue,
               this.authenticationContext.getLoggedInUser());
      for (ReopenRateInfo rri : ls) {
        if (reopenRateList.contains(rri)) {
          int index = reopenRateList.indexOf(rri);
          ReopenRateInfo crri = (ReopenRateInfo)reopenRateList.get(index);
          crri.addCounts(rri);
          crri.getReopenRate();
          reopenRateList.set(index, crri);
        } else {
          rri.getReopenRate();
          reopenRateList.add(rri);
        }
      }
    }
    return Response.ok(new ReopenRateList(reopenRateList, projectName, projectKey)).cacheControl(
      CacheControl.NO_CACHE).build();
  }

  private static List<ReopenRateInfo> getReopeningRateByUser(Issue issue, ApplicationUser remoteUser)
  {
    ChangeHistoryManager changeHistoryManager =
            ComponentAccessor.getChangeHistoryManager();
    List reopenRateList = new ArrayList();
    try {
      List changes = changeHistoryManager.getChangeHistoriesForUser(issue, remoteUser);
      ApplicationUser resolvedUser = null;
      for (int i = 0; i < changes.size(); i++) {
        ChangeHistory ch = (ChangeHistory)changes.get(i);
        List changeItems = ch.getChangeItems();
        for (int j = 0; j < changeItems.size(); j++) {
          GenericEntity changeItem = 
            (GenericEntity)changeItems
            .get(j);
          String ciField = changeItem.containsKey("field") ? 
            changeItem.getString("field") : 
            "";
          if (!"resolution".equals(ciField))
            continue;
          Object newValue = changeItem.get("newvalue");
          Object oldValue = changeItem.get("oldvalue");
          if ((newValue == null) && (oldValue != null)) {
            ReopenRateInfo rri = new ReopenRateInfo(
              resolvedUser.getName(), resolvedUser.getDisplayName(), 1, 0, 0);
            if (!reopenRateList.contains(rri))
              rri.addIssueCount(1);
            reopenRateList.add(rri);
          }
          else if ((newValue != null) && (oldValue == null)) {
            resolvedUser = ch.getAuthorObject();
            ReopenRateInfo rri = new ReopenRateInfo(
              resolvedUser.getName(), resolvedUser.getDisplayName(), 0, 1, 0);
            if (!reopenRateList.contains(rri))
              rri.addIssueCount(1);
            reopenRateList.add(rri);
          }
        }
      }
    }
    catch (Exception e) {
      logger.error("exception while getting actions", e);
    }

    return reopenRateList;
  }

  @XmlRootElement
  public static class ReopenRateInfo
  {

    @XmlElement
    private String userName;

    @XmlElement
    private String userLink;

    @XmlElement
    private int resolvedCount;

    @XmlElement
    private int reopenCount;

    @XmlElement
    private int issueCount;

    @XmlElement
    private int reopenRate;

    private ReopenRateInfo()
    {
    }

    ReopenRateInfo(String userName, String userLink, int reopenCount, int resolvedCount, int issueCount)
    {
      this.userName = userName;
      this.userLink = userLink;
      this.reopenCount = reopenCount;
      this.resolvedCount = resolvedCount;
      this.issueCount = issueCount;
    }

    public String getUserName()
    {
      return this.userName;
    }

    public String getUserLink() {
      return this.userLink;
    }

    public int getResolvedCount() {
      return this.resolvedCount;
    }

    public int getReopenCount() {
      return this.reopenCount;
    }

    public int getIssueCount() {
      return this.issueCount;
    }

    public int getReopenRate() {
      if (this.resolvedCount != 0)
        this.reopenRate = (this.reopenCount * 100 / this.resolvedCount);
      else
        this.reopenRate = 0;
      return this.reopenRate;
    }

    public int addReopenCount(int addvalue) {
      this.reopenCount += addvalue;
      return this.reopenCount;
    }

    public int addResovledCount(int addvalue) {
      this.resolvedCount += addvalue;
      return this.resolvedCount;
    }

    public int addIssueCount(int addvalue) {
      this.issueCount += addvalue;
      return this.issueCount;
    }

    public void addCounts(ReopenRateInfo inObj) {
      addIssueCount(inObj.getIssueCount());
      addReopenCount(inObj.getReopenCount());
      addResovledCount(inObj.getResolvedCount());
    }

    public boolean equals(Object obj)
    {
      ReopenRateInfo inObj = (ReopenRateInfo)obj;
      return inObj.getUserName().equals(getUserName());
    }
  }

  @XmlRootElement
  public static class ReopenRateList
  {

    @XmlElement
    Collection<ReopenRateResource.ReopenRateInfo> reopenRateList;

    @XmlElement
    String projectName;

    @XmlElement
    String projectKey;

    private ReopenRateList()
    {
    }

    ReopenRateList(Collection<ReopenRateResource.ReopenRateInfo> reopenRateList, String projectName, String projectKey)
    {
      this.reopenRateList = reopenRateList;
      this.projectName = projectName;
      this.projectKey = projectKey;
    }

    Collection<ReopenRateResource.ReopenRateInfo> getReopenRateList() {
      return this.reopenRateList;
    }
  }
}