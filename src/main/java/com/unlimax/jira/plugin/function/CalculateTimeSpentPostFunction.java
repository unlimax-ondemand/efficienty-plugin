package com.unlimax.jira.plugin.function;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.unlimax.jira.plugin.workflow.WorkflowUtils;
import java.sql.Timestamp;
import java.util.Map;

public class CalculateTimeSpentPostFunction extends AbstractJiraFunctionProvider
{
  public final void execute(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException
  {
    String fieldKey = (String)args.get("field.name");
    try
    {
      CustomField field = getFieldFromKey(fieldKey);
      MutableIssue issue = getIssue(transientVars);
      long tl = System.currentTimeMillis() - issue.getCreated().getTime();
      IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

      WorkflowUtils.setFieldValue(issue, field, String.valueOf(tl / 1000L / 3600L), changeHolder);
    } catch (Exception e) {
      String message = "Unable set timespent value!";

      throw new WorkflowException(message);
    }
  }

  private CustomField getFieldFromKey(String key)
  {
    FieldManager fieldManager = ComponentAccessor.getFieldManager();
    CustomField field;
    if (fieldManager.isCustomField(key))
      field = fieldManager.getCustomField(key);
    else {
      field = null;
    }
    if (field == null) {
      throw new IllegalArgumentException(
        "Unable to find field '" + key + "'");
    }

    return field;
  }
}